import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http'
import { NgModule } from '@angular/core';
import { SuiModule } from 'ng2-semantic-ui';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { MovieDetailsComponent } from './movie-details/movie-details.component';
import { MoviesComponent } from './movies/movies.component';

const appRoutes: Routes = [
  { path: 'movie-info', component: MoviesComponent },
  { path: 'movie-info/:id', component: MovieDetailsComponent },
  { path: '', component: MainComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    MovieDetailsComponent,
    MoviesComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    SuiModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
