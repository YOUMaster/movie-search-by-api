import { Component, OnDestroy } from '@angular/core';
import { RequestService } from '../shared/services/request.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnDestroy {
  public searchResult;
  public searchValue: string = '';
  public subscription: Subscription;

  constructor(
    private _requestService: RequestService
  ) { }

  public nextPage(pageNumber: number): void {
    this.getMovieList(pageNumber);
  }

  public update(e, pageNumber?: number): void {
    this.searchValue = e.target.value;
    this.getMovieList(pageNumber);
  }

  public getMovieList(pageNumber: number): void {
    let result = this._requestService.searchMovie(this.searchValue, pageNumber);

    if (result) {
      this.subscription = result.subscribe(item => {
        this.searchResult = item;
      });
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
