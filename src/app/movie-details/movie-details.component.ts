import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { RequestService } from '../shared/services/request.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.css']
})
export class MovieDetailsComponent implements OnInit, OnDestroy {

  public id: number;
  public movieInfo;
  public subscription: Subscription;

  constructor(
    private _route: ActivatedRoute,
    private _requestService: RequestService
  ) { }

  ngOnInit() {
    this.id = +this._route.snapshot.params['id'];

    this._route.params.subscribe((params: Params) => {
      this.id = +params['id'];
    });

    this.subscription = this._requestService.getMovieDetails(this.id ).subscribe(movieInfo => {
      this.movieInfo = movieInfo;
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
