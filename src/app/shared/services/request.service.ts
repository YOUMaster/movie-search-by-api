import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RequestService {
  private APIkey = 'de1f2ef0a0469870de398597a7e02c30';

  constructor(
    private http: HttpClient
  ) {}

  public searchMovie(query: string, page?: number): Observable<any> {
    let pageString = page ? '&page=' + page : '';
    if (query && query.trim() !== "") {
      let dataURI = 'https://api.themoviedb.org/3/search/movie?api_key=' + this.APIkey + '&query=' + query + pageString;
      return this.http.get(dataURI);
    }
  }

  public getMovieDetails(id: number): Observable<any> {
    let dataURI = 'https://api.themoviedb.org/3/movie/' + id + '?api_key=' + this.APIkey;
    return this.http.get(dataURI);
  }
}
